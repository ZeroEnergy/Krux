package com.ceedlabs.krux

import com.ceedlabs.krux.module.ModuleManager
import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.bind
import com.github.salomonbrys.kodein.multiton
import org.slf4j.LoggerFactory
import java.nio.file.Files

object Krux {
    val kodein = Kodein {
        bind() from multiton { clazz: Class<*> -> LoggerFactory.getLogger(modManager.moduleDefinitions.first { it.mainClass == clazz.canonicalName }.displayName) }
        bind() from multiton { clazz: Class<*> -> modManager.moduleDefinitions.first { it.mainClass == clazz.canonicalName } }
    }

    private val modManager: ModuleManager = ModuleManager()
    internal val logger = LoggerFactory.getLogger("Krux")!!

    @JvmStatic
    fun main(args: Array<String>) {
        logger.info("Starting Krux")
        KruxEngine.initGlfw()
        if (!Files.exists(modManager.moduleFolderPath)) Files.createDirectories(modManager.moduleFolderPath)
        modManager.modules.forEach { it.onLoad(kodein) }
        KruxEngine.play()
        logger.info("Stopping Krux")
        modManager.modules.forEach { it.onUnload() }
        KruxEngine.terminateGlfw()
    }
}