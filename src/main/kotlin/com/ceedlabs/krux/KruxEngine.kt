package com.ceedlabs.krux

import com.ceedlabs.krux.entity.Entity
import org.joml.Matrix4f
import org.lwjgl.BufferUtils
import org.lwjgl.Version
import org.lwjgl.glfw.Callbacks
import org.lwjgl.glfw.GLFW
import org.lwjgl.glfw.GLFW.glfwPollEvents
import org.lwjgl.glfw.GLFW.glfwSwapBuffers
import org.lwjgl.glfw.GLFWErrorCallback
import org.lwjgl.opengl.GL
import org.lwjgl.opengl.GL11
import org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT
import org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT
import org.lwjgl.opengl.GL11.GL_FLOAT
import org.lwjgl.opengl.GL11.GL_TRIANGLES
import org.lwjgl.opengl.GL11.GL_TRUE
import org.lwjgl.opengl.GL11.glClear
import org.lwjgl.opengl.GL11.glDrawArrays
import org.lwjgl.opengl.GL15.GL_ARRAY_BUFFER
import org.lwjgl.opengl.GL15.GL_STATIC_DRAW
import org.lwjgl.opengl.GL15.glBindBuffer
import org.lwjgl.opengl.GL15.glBufferData
import org.lwjgl.opengl.GL15.glDeleteBuffers
import org.lwjgl.opengl.GL15.glGenBuffers
import org.lwjgl.opengl.GL20.GL_COMPILE_STATUS
import org.lwjgl.opengl.GL20.GL_FRAGMENT_SHADER
import org.lwjgl.opengl.GL20.GL_LINK_STATUS
import org.lwjgl.opengl.GL20.GL_VERTEX_SHADER
import org.lwjgl.opengl.GL20.glAttachShader
import org.lwjgl.opengl.GL20.glCompileShader
import org.lwjgl.opengl.GL20.glCreateProgram
import org.lwjgl.opengl.GL20.glCreateShader
import org.lwjgl.opengl.GL20.glDeleteProgram
import org.lwjgl.opengl.GL20.glDeleteShader
import org.lwjgl.opengl.GL20.glEnableVertexAttribArray
import org.lwjgl.opengl.GL20.glGetAttribLocation
import org.lwjgl.opengl.GL20.glGetProgramInfoLog
import org.lwjgl.opengl.GL20.glGetProgrami
import org.lwjgl.opengl.GL20.glGetShaderInfoLog
import org.lwjgl.opengl.GL20.glGetShaderi
import org.lwjgl.opengl.GL20.glGetUniformLocation
import org.lwjgl.opengl.GL20.glLinkProgram
import org.lwjgl.opengl.GL20.glShaderSource
import org.lwjgl.opengl.GL20.glUniformMatrix4fv
import org.lwjgl.opengl.GL20.glUseProgram
import org.lwjgl.opengl.GL20.glVertexAttribPointer
import org.lwjgl.opengl.GL30
import org.lwjgl.opengl.GL30.glBindFragDataLocation
import org.lwjgl.opengl.GL30.glDeleteVertexArrays
import org.lwjgl.system.MemoryStack
import org.lwjgl.system.MemoryUtil
import java.io.File

object KruxEngine {

    internal val entities = mutableListOf<Entity>()

    private var window = 0L

    private val WIDTH = 300
    private val HEIGHT = 300

    private var vao: Int = 0
    private var vbo = 0
    private var vertexShader = 0
    private var fragmentShader = 0
    private var shaderProgram = 0

    internal fun play() {
        //FIXME: Move to some other place that handles game initialization
        MemoryStack.stackPush().use {
            val vertices = it.mallocFloat(3 * 6)
            vertices.put(-0.6f).put(-0.4f).put(0f).put(1f).put(0f).put(0f)
            vertices.put(0.6f).put(-0.4f).put(0f).put(0f).put(1f).put(0f)
            vertices.put(0f).put(0.6f).put(0f).put(0f).put(0f).put(1f)
            vertices.flip()

            vbo = glGenBuffers()
            glBindBuffer(GL_ARRAY_BUFFER, vbo)
            glBufferData(GL_ARRAY_BUFFER, vertices, GL_STATIC_DRAW)
        }

        vertexShader = glCreateShader(GL_VERTEX_SHADER)
        glShaderSource(vertexShader, File("default.vert").readLines().joinToString("\n"))
        glCompileShader(vertexShader)
        if (glGetShaderi(vertexShader, GL_COMPILE_STATUS) != GL_TRUE) throw RuntimeException(glGetShaderInfoLog(vertexShader))

        fragmentShader = glCreateShader(GL_FRAGMENT_SHADER)
        glShaderSource(fragmentShader, File("default.frag").readLines().joinToString("\n"))
        glCompileShader(fragmentShader)
        if (glGetShaderi(fragmentShader, GL_COMPILE_STATUS) != GL_TRUE) throw RuntimeException(glGetShaderInfoLog(fragmentShader))

        shaderProgram = glCreateProgram()
        glAttachShader(shaderProgram, vertexShader)
        glAttachShader(shaderProgram, fragmentShader)
        glBindFragDataLocation(shaderProgram, 0, "fragColor")
        glLinkProgram(shaderProgram)
        if (glGetProgrami(shaderProgram, GL_LINK_STATUS) != GL_TRUE) throw RuntimeException(glGetProgramInfoLog(shaderProgram))

        glUseProgram(shaderProgram)

        val floatSize = 4
        val posAttrib = glGetAttribLocation(shaderProgram, "position")
        glEnableVertexAttribArray(posAttrib)
        glVertexAttribPointer(posAttrib, 3, GL_FLOAT, false, 6 * floatSize, 0)

        val colAttrib = glGetAttribLocation(shaderProgram, "color")
        glEnableVertexAttribArray(colAttrib)
        glVertexAttribPointer(colAttrib, 3, GL_FLOAT, false, 6 * floatSize, (3 * floatSize).toLong())

        val uniModel = glGetUniformLocation(shaderProgram, "model")
        val model = Matrix4f()
        glUniformMatrix4fv(uniModel, false, model.get(BufferUtils.createFloatBuffer(16)))

        val uniView = glGetUniformLocation(shaderProgram, "view")
        val view = Matrix4f()
        glUniformMatrix4fv(uniView, false, view.get(BufferUtils.createFloatBuffer(16)))

        val uniProjection = glGetUniformLocation(shaderProgram, "projection")
        val ratio = WIDTH / HEIGHT.toFloat()
        val projection = Matrix4f().setOrtho(-ratio, ratio, -1f, 1f, 0.1f, 100f)
        glUniformMatrix4fv(uniProjection, false, projection.get(BufferUtils.createFloatBuffer(16)))

        while (!windowShouldClose()) {
            takeInput()
            update()
            render()
        }
    }

    private fun takeInput() {
        glfwPollEvents()
    }

    private fun update() {}
    private fun render() {
        glClear(GL_COLOR_BUFFER_BIT or GL_DEPTH_BUFFER_BIT)
        glDrawArrays(GL_TRIANGLES, 0, 3)
        glfwSwapBuffers(window)
    }

    internal fun initGlfw() {
        Krux.logger.info("Running on LWJGL ${Version.getVersion()}!")

        GLFWErrorCallback.createPrint(System.err).set()

        if (!GLFW.glfwInit()) throw IllegalStateException("Unable to initialize GLFW")

        GLFW.glfwDefaultWindowHints()
        GLFW.glfwWindowHint(GLFW.GLFW_VISIBLE, GLFW.GLFW_FALSE)
        GLFW.glfwWindowHint(GLFW.GLFW_RESIZABLE, GLFW.GLFW_TRUE)

        window = GLFW.glfwCreateWindow(WIDTH, HEIGHT, "Hello World!", MemoryUtil.NULL, MemoryUtil.NULL)
        if (window == MemoryUtil.NULL) throw RuntimeException("Failed to create the GLFW window")

        GLFW.glfwSetKeyCallback(window) { window, key, _, action, _ ->
            if (key == GLFW.GLFW_KEY_ESCAPE && action == GLFW.GLFW_RELEASE)
                GLFW.glfwSetWindowShouldClose(window, true)
        }

        MemoryStack.stackPush().use { stack ->
            val pWidth = stack.mallocInt(1)
            val pHeight = stack.mallocInt(1)

            GLFW.glfwGetWindowSize(window, pWidth, pHeight)

            val vidmode = GLFW.glfwGetVideoMode(GLFW.glfwGetPrimaryMonitor())

            GLFW.glfwSetWindowPos(
                    window,
                    (vidmode.width() - pWidth.get(0)) / 2,
                    (vidmode.height() - pHeight.get(0)) / 2
            )
        }

        GLFW.glfwMakeContextCurrent(window)
        GLFW.glfwSwapInterval(1)
        GLFW.glfwShowWindow(window)

        GL.createCapabilities()
        GL11.glClearColor(0.0f, 0.0f, 0.0f, 0.0f)

        vao = GL30.glGenVertexArrays()
        GL30.glBindVertexArray(vao)
    }

    internal fun terminateGlfw() {
        glDeleteVertexArrays(vao)
        glDeleteBuffers(vbo)
        glDeleteShader(vertexShader)
        glDeleteShader(fragmentShader)
        glDeleteProgram(shaderProgram)

        Callbacks.glfwFreeCallbacks(window)
        GLFW.glfwDestroyWindow(window)
        GLFW.glfwTerminate()
        GLFW.glfwSetErrorCallback(null).free()
    }

    private fun windowShouldClose() = GLFW.glfwWindowShouldClose(window)

}