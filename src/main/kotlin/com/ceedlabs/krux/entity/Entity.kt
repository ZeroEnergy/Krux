package com.ceedlabs.krux.entity

import com.ceedlabs.krux.KruxEngine
import java.nio.FloatBuffer
import java.util.UUID

data class Entity(val components: Set<Component>, val id: UUID = UUID.randomUUID()) {
    init {
        KruxEngine.entities.add(this)
    }
}

interface Component

data class ModelComponent(var model: FloatBuffer) : Component