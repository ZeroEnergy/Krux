package com.ceedlabs.krux.module

import com.github.salomonbrys.kodein.Kodein

interface IModule {
    fun onLoad(kodein: Kodein)
    fun onUnload()
}