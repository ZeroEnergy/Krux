package com.ceedlabs.krux.module

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
data class ModuleDefinition(
        val displayName: String,
        val mainClass: String)