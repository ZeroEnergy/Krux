package com.ceedlabs.krux.module

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.io.File
import java.net.JarURLConnection
import java.net.URL
import java.net.URLClassLoader
import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path

class ModuleManager {
    val moduleFolderPath: Path by lazy { File("${System.getProperty("user.home")}/.krux/modules").toPath()!! }

    private val modulePaths: Iterable<Path> by lazy {
        Files.newDirectoryStream(moduleFolderPath).filter {
            !Files.isSameFile(it, moduleFolderPath) && Files.isRegularFile(it) && FileSystems.getDefault().getPathMatcher("glob:**.jar").matches(it)
        }
    }

    val moduleDefinitions: Collection<ModuleDefinition> by lazy {
        modulePaths
                .map { (URL("jar:file:${it.toAbsolutePath()}!/krux-mod.json").openConnection() as JarURLConnection).inputStream }
                .map { it.use { jacksonObjectMapper().readValue<ModuleDefinition>(it) } }
    }

    val modules: Iterable<IModule> by lazy {
        moduleDefinitions
                .map {
                    Class.forName(it.mainClass, true,
                            URLClassLoader.newInstance(modulePaths.map { it.toUri().toURL() }
                                    .toTypedArray(), ClassLoader.getSystemClassLoader()))
                }
                .map { it.newInstance() as IModule }
    }
}